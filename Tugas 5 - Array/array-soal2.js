function range(startNum, finishNum, step){
    var x=[];
    for (i = startNum; i <= finishNum; i += step){
        x.push(i);
    } for (i = startNum; i >= finishNum; i -= step){
        x.push(i);
    } if (startNum == null || finishNum == null || step == null){
        return -1;
    } return x;
} 

console.log(range(1,10,2))
console.log(range(11,23,3))
console.log(range(5,2,1))
console.log(range(29,2,4))