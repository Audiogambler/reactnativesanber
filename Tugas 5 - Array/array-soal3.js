function sum(startNum, finishNum, step){
    var x=[];
    var deret=0;

    if (step == null){
        step=1;
        if(finishNum == null){
            finishNum=0;
        }
    }
    
    for (i = startNum; i <= finishNum; i += step){
        deret=deret+i;
        x.push(deret);
    } for (i = startNum; i >= finishNum; i -= step){
        deret=deret+i;
        x.push(deret);
    }  return deret;
} 

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0